# ThemysSuperBuild

This project furnishes a **relocatable binary distribution** of [`Themys`](https://gitlab.com/themys/themys) and [`Readers`](https://giltab.com/themys/readers) projects and all their dependencies.
It uses `CMake` superbuild approach.

The direct dependencies are `ParaView`, which is itself built with [`ParaView-Superbuild`](https://gitlab.kitware.com/paraview/paraview-superbuild) project, and [`Hercule`](https://gitlab.com/hercule-io/public/hercule).

Two methods are available to obtain the relocatable binary distribution archive:

- the first one is through `CMake` commands;
- the second one is through `Docker` commands.

## Foreword

In all this document the following variables are used: 

- `<THEMYS_SUPERBUILD_VERSION>` is the version number of this project;
- `<PARAVIEW_GIT_GROUP>` is the group of the desired Gitlab repository for `ParaView`;
- `<PARAVIEW_GIT_TAG>` is the desired tag or commit number that will be used to build `ParaView`;
- `<THEMYS_GIT_TAG>` is the desired tag or commit number that will be used to build `Themys`;
- `<THEMYS_BUILD_DOCUMENTATION>` is the boolean that triggers the build of Themys documentation;
- `<READERS_GIT_TAG>` is the desired tag or commit number that will be used to build `Readers`;
- `<CMAKE_BUILD_TYPE>` is the desired CMake build type for all projects.

## CMake commands

### Build

First step is to clone the repository:

~~~bash
git clone https://gitlab.com/themys/themyssuperbuild.git
~~~

To have a version of `Hercule` that is relocatable, it is necessary to patch it. The patch process is handled by `CMake` but it 
is necessary to copy the `hercule_her_install.patch` file into the source directory:

~~~bash
cd themyssuperbuild
cp patch/hercule_her_install.patch .
~~~

Then it is necessary to create a `build` directory and configure the project:

~~~bash
mkdir build
cd build
cmake -GNinja \
-DTHEMYS_SUPERBUILD_VERSION=$THEMYS_SUPERBUILD_VERSION \
-DPARAVIEW_GIT_GROUP=$PARAVIEW_GIT_GROUP \
-DPARAVIEW_GIT_TAG=$PARAVIEW_GIT_TAG \
-DTHEMYS_GIT_TAG=$THEMYS_GIT_TAG \
-DTHEMYS_BUILD_DOCUMENTATION=$THEMYS_BUILD_DOCUMENTATION \
-DREADERS_GIT_TAG=$READERS_GIT_TAG \
-DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE \
../themyssuperbuild
~~~

Once `CMake` configuration is done, the build is triggered by:

~~~bash
cmake --build .
~~~

### Packaging the archive

Before launching the command that will produce a relocatable archive it is necessary to copy the `pre_build_script.cmake` script into the 
build directory:

~~~bash
cd build
cp cmake/pre_build_script.cmake .
~~~

This script will configure the `themys` executable so that it can find the `readers` plugin. It will also fix `Hercule` install
so that it is truly relocatable.

Then the packaging command can be launched:

~~~bash
cpack -G TGZ --config CPackConfig.cmake
~~~

At the end of this command, an archive named `ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux.tar.gz` is created side by side with the `ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux.tar.gz.sha256` file. This last one contains the sha256 checksum of the archive. 

Archive integrity can be controlled by typing:

~~~bash
sha256sum -c ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux.tar.gz.sha256
~~~

If the integrity is verified the output of the command is

~~~bash
ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux.tar.gz: Réussi
~~~

## Docker method

While the `CMake` approach is the classical way, using `Docker` is probably easier to obtain the relocatable archive.

### Build

First step is to clone the repository:

~~~bash
git clone https://gitlab.com/themys/themyssuperbuild.git
~~~

Then the following command should be typed:

~~~bash
cd themyssuperbuild
docker build --network=host -t themys_superbuild:build \
--build-arg="THEMYS_SUPERBUILD_VERSION=<THEMYS_SUPERBUILD_VERSION>" \
--build-arg="PARAVIEW_GIT_GROUP=<PARAVIEW_GIT_GROUP>" \
--build-arg="PARAVIEW_GIT_TAG=<PARAVIEW_GIT_TAG>" \
--build-arg="THEMYS_GIT_TAG=<THEMYS_GIT_TAG>" \
--build-arg="THEMYS_BUILD_DOCUMENTATION=<THEMYS_BUILD_DOCUMENTATION>" \
--build-arg="READERS_GIT_TAG=<READERS_GIT_TAG>" \
--build-arg="CMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>" \
.
~~~

If `Docker` is used behing a proxy, then the following argument should be added to the previous command:

~~~bash
--build-arg="PROXY_ADDRESS=http://<proxy_address>:<proxy_port>"
~~~

where `<proxy_address>` and `<proxy_port>` have to be replaced by correct values.

The option `--no-cache` can also be added in order to not reuse the cache of preceding builds.

### Archive retrieval

At the end of the previous command, the archive has been created but it is stored inside the `Docker` image created (named `themys_superbuild:build` in this example).

To retrieve it in the host filesystem, a container running this image should be created:

~~~bash
docker run -e DISPLAY=:0  --device=/dev/dri:/dev/dri --name archive_retriever themys_superbuild:build
~~~

then the archive can be copied into the host filesystem through:

~~~bash
cd /tmp/
docker cp archive_retriever:/opt/build/ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux.tar.gz .
~~~

Once the previous command has run, the archive is available under the `/tmp` directory.

## CI

The previous `Docker` approach is the method used by the CI to create the archive.
The CI pipeline is triggered by the creation of a new tag and the versions of `ParaView`, `Themys` and `Readers` are set in the `.gitlab-ci.yml` file:

~~~
variables:
  ...
  PARAVIEW_GIT_TAG: 8f4e48bc
  THEMYS_GIT_TAG: 1.0.7
  READERS_GIT_TAG: 1.0.8
  ...
~~~

The CI has two stages:

- build: builds the relocatable binary archive using `Docker`
- upload: uploads the obtained archive into the [`ThemysSuperBuild`'s package registry](https://gitlab.com/themys/themyssuperbuild/-/packages).

## Archive usage

To use the relocatable binaries in the archive, just unzip the archive, for example in the `$HOME` directory, and lauch the binary in the `bin` directory:

~~~bash
cd $HOME
tar -zxvf ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux.tar.gz
cd ThemysSuperBuild-PARAVIEW<PARAVIEW_GIT_TAG>_THEMYS<THEMYS_GIT_TAG>_READERS<READERS_GIT_TAG>-Linux/
./bin/themys
~~~

It must be noted that `Qt5` should be installed on the system where Themys is extracted.

It is possible to use client/server abilities of `themys` by launching

~~~bash
./bin/mpiexec -n 4 ./bin/pvserver
~~~

where `4` is the numbers of `pvserver` that will be launched.

This last command prints the address to connect to using the `themys` client:

~~~bash
Waiting for client...
Connection URL: cs://<HOSTNAME>:11111
Accepting connection(s): <HOSTNAME>:11111
~~~

Thus, launch the client and ask it to connect to the servers with the following commands:

~~~bash
./bin/themys --url=cs://<HOSTNAME>:11111
~~~

The graphical user interface will warn that some plugins loaded on the client side are not loaded on the server side.
While it is possible to use the GUI to load such plugins on the server side, it is probably better to adapt a bit the previous command
to automatically load those plugins by using the options `--plugin-search-paths` and `plugins`.

~~~bash
./bin/mpiexec -n 4 ./bin/pvserver --plugin-search-paths=/path/to/the/directory/of/myplugin.so --plugins=myplugin
~~~

## Devs and debug tips

In order to understand what goes wrong when building the image, you can comment the line which causes the bug and all the lines below in the [DockerFile].
Then, you can run the partial image using this command :

~~~bash
docker run --network=host -it -e DISPLAY=:0  --device=/dev/dri:/dev/dri --name archive_retriever themys_superbuild:build
~~~

Now, you can run one by one the commented lines in the container in order to debug.

<!-- If you want to pack the sources along with the executables in debug, you can run in the container :

~~~bash
cpack -G TGZ --config CPackSourceConfig.cmake
~~~ -->
