#---------------------------------------
# First stage is to install a qt in the system
# to avoid using the one embedded in PV.
#
# This embedded version does compile right
# but when running Themys in TB-AMR mode, click
# On the Filter menu leads to a crash
# 
# We do intall in a specific stage in order to not
# install unwanted stuff in the superbuild stage
FROM ubuntu:18.04 AS install_qt
ARG PROXY_ADDRESS=""

# Kept across stages
ENV QT_VERSION="5.12.12"
ENV CC=/usr/bin/gcc
ENV CXX=/usr/bin/g++

RUN if [[ -n "${PROXY_ADDRESS}" ]]; \
    then echo "Acquire::http::User-Agent \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36\";\
    Acquire::http::proxy \"${PROXY_ADDRESS}\";\
    Acquire::https::proxy \"${PROXY_ADDRESS}\";" > /etc/apt/apt.conf.d/98user-agent-proxy;\
    else\
    echo "Not on site!";\
    fi

RUN apt-get update

COPY qt-everywhere-src-${QT_VERSION}.tar.xz /tmp/

WORKDIR /tmp

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    make \
    g++-8 \ 
    xz-utils \
    '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev \
    libxkbcommon-dev libxkbcommon-x11-dev libglvnd-dev \
    python \
    fontconfig \
    libfontconfig-dev \
    libxcomposite-dev \
    libxext-dev \
    libxcursor-dev \
    libdbus-1-dev \
    libxtst-dev \
    libnss3-dev \
    libfreetype6-dev

# Qt5 looks for gcc/g++ in $PATH and does not seem to care of CC and CXX env variables
RUN ln -s /usr/bin/gcc-8 /usr/bin/gcc

RUN ln -s /usr/bin/g++-8 /usr/bin/g++

RUN tar -xvf qt-everywhere-src-${QT_VERSION}.tar.xz

WORKDIR /tmp/qt-everywhere-src-${QT_VERSION}

RUN ./configure --shared --prefix=/opt/qt-${QT_VERSION} -opensource -qpa xcb -nomake tests -nomake examples -fontconfig

RUN make -j 20

RUN make install
#------------------------------------------------------------------------------
# Take ubuntu:18.04 to have access to gcc-8 and also to have a glibc compatible
# with internal needs
# With version above gcc-10, qt5 needs a patch to work, we dont want this for now
FROM ubuntu:18.04 AS superbuild
ARG PROXY_ADDRESS=""
ARG THEMYS_SUPERBUILD_VERSION="1.0.0"
ARG PARAVIEW_GIT_GROUP="paraview"
ARG PARAVIEW_GIT_TAG="master"
ARG THEMYS_GIT_TAG="master"
ARG THEMYS_BUILD_DOCUMENTATION="ON"
ARG READERS_GIT_TAG="master"
ARG CMAKE_BUILD_TYPE="RelWithDebInfo"

RUN if [[ -n "${PROXY_ADDRESS}" ]]; \
    then echo "Acquire::http::User-Agent \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36\";\
    Acquire::http::proxy \"${PROXY_ADDRESS}\";\
    Acquire::https::proxy \"${PROXY_ADDRESS}\";" > /etc/apt/apt.conf.d/98user-agent-proxy;\
    else\
    echo "Not on site!";\
    fi

RUN apt-get update

# Download the minimal dependencies. 
# For example we don't want python so that we are sure to use paraview's python for further
# install such as Hercule
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    make \
    g++-8 \ 
    git \
    patchelf \
    file \
    nano \
    patch \
    '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev \
    libxkbcommon-dev libxkbcommon-x11-dev libglvnd-dev \
    libboost-dev \
    swig \
    ca-certificates \
    curl \
    libssl-dev \
    fontconfig \
    libfontconfig-dev \
    libxcomposite-dev \
    libxext-dev \
    libxcursor-dev \
    libdbus-1-dev \
    libxtst-dev \
    libnss3-dev \
    libfreetype6-dev

WORKDIR /opt

# The scripts in the scripts directory are taken from paraview-superbuild and adapted to our needs
# They allow us to download latest cmake and ninja version that are necessary for paraview-superbuild
COPY scripts ./scripts

RUN ./scripts/cmake.sh

RUN ./scripts/ninja.sh

ENV PATH=/opt/external_tools:/opt/external_tools/cmake/bin:$PATH

# Qt5 looks for gcc/g++ in $PATH and does not seem to care of CC and CXX env variables
RUN ln -s /usr/bin/gcc-8 /usr/bin/gcc

RUN ln -s /usr/bin/g++-8 /usr/bin/g++

ENV CC=/usr/bin/gcc

ENV CXX=/usr/bin/g++

WORKDIR /opt/themys_superbuild

COPY CMakeLists.txt .

COPY patch/hercule_her_install.patch .

ENV QT_VERSION="5.12.12"
COPY --from=install_qt /opt/qt-${QT_VERSION} /opt/qt-${QT_VERSION}

ENV LD_LIBRARY_PATH=/opt/qt-${QT_VERSION}/lib:${LD_LIBRARY_PATH}

WORKDIR /opt/build

RUN cmake -GNinja \
    -DTHEMYS_SUPERBUILD_VERSION=$THEMYS_SUPERBUILD_VERSION \
    -DPARAVIEW_GIT_GROUP=$PARAVIEW_GIT_GROUP \
    -DPARAVIEW_GIT_TAG=$PARAVIEW_GIT_TAG \
    -DTHEMYS_GIT_TAG=$THEMYS_GIT_TAG \
    -DTHEMYS_BUILD_DOCUMENTATION=$THEMYS_BUILD_DOCUMENTATION \
    -DREADERS_GIT_TAG=$READERS_GIT_TAG \
    -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE \
    -DCMAKE_PREFIX_PATH=/opt/qt-${QT_VERSION}/lib/cmake/Qt5 \
    /opt/themys_superbuild

RUN cmake --build . --target ParaViewSuperBuild > /opt/build/paraview_super_build.log

RUN cmake --build . --target Hercule > /opt/build/hercule.log

RUN cmake --build . --target Readers > /opt/build/readers.log

# Themys is built with documentation enabled. Thus it needs sphinx.
# Install it in a virtual env of ParaView's python.
# Do not use apt-get because it would install a python3 system
# Use system-site-packages otherwise mpi4py would be missing
RUN pythonexe=$(find . -wholename .*/bin/python3) && $pythonexe -m venv --system-site-packages /opt/python_env4doc

RUN . /opt/python_env4doc/bin/activate && pip install sphinx

RUN . /opt/python_env4doc/bin/activate && cmake --build . --target Themys > /opt/build/themys.log

RUN . /opt/python_env4doc/bin/activate && cmake --build . > /opt/build/all.log

WORKDIR /opt/themys_superbuild

COPY cmake/pre_build_script.cmake .

WORKDIR /opt/build

RUN cpack -G TGZ --config CPackConfig.cmake
