# First we modify the paraview config to load the CEAHerculeReaders plugin automatically
set(extras_content "<?xml version='1.0'?><Plugins><Plugin name='CEAHerculeReaders' auto_load='1'/></Plugins>")

message(STATUS "Appending ../lib/themys/extras.xml to ${CPACK_TEMPORARY_INSTALL_DIRECTORY}/bin/themys.conf")
file(APPEND ${CPACK_TEMPORARY_INSTALL_DIRECTORY}/bin/themys.conf ../lib/themys/extras.xml)
message(STATUS "Writing file ${CPACK_TEMPORARY_INSTALL_DIRECTORY}/lib/themys/extras.xml")
file(WRITE ${CPACK_TEMPORARY_INSTALL_DIRECTORY}/lib/themys/extras.xml ${extras_content})

# Second, Hercule is not yet fully relocatable.
# Patch the hercule_gme_prepare_index script to not depend on install path but on executable path"
set(script_name "hercule_gme_prepare_index.sh")
message(STATUS "Fixing ${script_name} script")

file(READ ${CPACK_TEMPORARY_INSTALL_DIRECTORY}/bin/private/${script_name} SCRIPT_CONTENT)
set(replacement
"GME_BUILD_INDEX_CMD=$(dirname $(realpath $0))/gme_build_index\n\
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$(dirname $(realpath $0))/../../lib"
)
string(REGEX REPLACE "GME_BUILD_INDEX_CMD=[^ \t\r\n]*/gme_build_index" ${replacement} SCRIPT_MODIFIED_CONTENT "${SCRIPT_CONTENT}")
file(WRITE ${CPACK_TEMPORARY_INSTALL_DIRECTORY}/bin/private/${script_name} "${SCRIPT_MODIFIED_CONTENT}")
